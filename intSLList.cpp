//************************  intSLList.cpp  **************************

#include <iostream>
#include "intSLList.h"

using namespace std;

IntSLList::~IntSLList() {
    for (IntSLLNode *p; !isEmpty(); ) {
        p = head->next;
        delete head;
        head = p;
    }
}

void IntSLList::addToHead(int el) {
    head = new IntSLLNode(el,head);
    if (tail == 0)
       tail = head;
}

void IntSLList::addToTail(int el) {
    if (tail != 0) {      // if list not empty;
         tail->next = new IntSLLNode(el);
         tail = tail->next;
    }
    else head = tail = new IntSLLNode(el);
}

int IntSLList::deleteFromHead() {
    int el = head->info;
    IntSLLNode *tmp = head;
    if (head == tail)     // if only one node on the list;
         head = tail = 0;
    else head = head->next;
    delete tmp;
    return el;
}

int IntSLList::deleteFromTail() {
    int el = tail->info;
    if (head == tail) {   // if only one node on the list;
         delete head;
         head = tail = 0;
    }
    else {                // if more than one node in the list,
         IntSLLNode *tmp; // find the predecessor of tail;
         for (tmp = head; tmp->next != tail; tmp = tmp->next);
         delete tail;
         tail = tmp;      // the predecessor of tail becomes tail;
         tail->next = 0;
    }
    return el;
}

void IntSLList::deleteNode(int el) {
    if (head != 0)                     // if non-empty list;
         if (head == tail && el == head->info) { // if only one
              delete head;                       // node on the list;
              head = tail = 0;
         }
         else if (el == head->info) {  // if more than one node on the list
              IntSLLNode *tmp = head;
              head = head->next;
              delete tmp;              // and old head is deleted;
         }
         else {                        // if more than one node in the list
              IntSLLNode *pred, *tmp;
              for (pred = head, tmp = head->next; // and a non-head node
                   tmp != 0 && !(tmp->info == el);// is deleted;
                   pred = pred->next, tmp = tmp->next);
              if (tmp != 0) {
                   pred->next = tmp->next;
                   if (tmp == tail)
                      tail = pred;
                   delete tmp;
              }
         }
}

bool IntSLList::isInList(int el) const {
    IntSLLNode *tmp;
    for (tmp = head; tmp != 0 && !(tmp->info == el); tmp = tmp->next);
    return tmp != 0;
}

void IntSLList::printAll() const {
    for (IntSLLNode *tmp = head; tmp != 0; tmp = tmp->next)
        cout << tmp->info << " ";
        cout << endl;
}

//My functions

bool IntSLList::isEmptyList() const {
    IntSLLNode *tmp = head;
    
    if(tmp == 0)
        return true;
    else
        return false;
    
}
/*
 @Returns true to indicate operation is completed. 
 */
bool IntSLList::deleteAll() const{
    
    if (this->isEmptyList()) {
        cout << "ERROR: List is empty.";
        return false;
    }
    else{
        this->~IntSLList();
    }
    return true;
}

int IntSLList::listcount(){
    int listSize = 0;
    
    IntSLLNode* origHead = head;
    
    while (head) {
        listSize++;
        head = head->next;
    }
    head = origHead;
    
    return listSize;
                            
}
/*
 Idea: add current element * multipler to tail, then delete current node.
 */
void IntSLList::multiply(int multipler){
    int listSize = this->listcount();
    IntSLLNode *curr;
    for (int i = 0; i < listSize; i++) {
        curr = head;
        this->addToTail(curr->info * multipler);
        head = head->next;
        delete curr;
    }
}

void IntSLList::appendList(IntSLList* list02){
    
    int list02Size = list02->listcount();
    IntSLLNode *curr;
    
    if (this->isEmptyList() && list02->isEmptyList()) {
        cout << "ERROR: Both list are empty." << endl;
    }
    
    else{
        for (int i = 0; i < list02Size; i++) {
            int val =list02->head->info;
            this->addToTail(val);
            curr = list02->head;
            list02->head = list02->head->next;
            delete curr;
        }
    }
}

int IntSLList::frequency(int val){
    int count = 0;
    
    if(this->isEmptyList())
        cout << "ERROR: This list is empty";
    else
    {
        for (IntSLLNode *tmp = head; tmp != 0; tmp = tmp->next){
            if(tmp->info == val)
                count++;
        }
    }
    return count;
}

void IntSLList::removeNod(int n,int k){
    
    IntSLLNode* temp, *curr, *prev;
    int index = 0;
    
    if (n > k) {
        cout << "lol";
        goto hell;
    }

    if(this->listcount() > k){
        prev = curr = head;
        
        while (curr != 0){
            if(index >= n && index <= k){
                
                temp = curr;
                curr = curr->next;
                prev->next = curr;
                delete temp;
            }
            else{
                prev = curr;
                curr = curr->next;
            }

            index++;
        }
    }
    else{
        cout << "ERROR: Out of scope." << endl;
    }

hell:
    int u = 1/0;
}
