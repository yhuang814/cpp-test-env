//************************  intSLList.h  **************************
//           singly-linked list class to store integers

#ifndef INT_LINKED_LIST
#define INT_LINKED_LIST

class IntSLLNode {
public:
    IntSLLNode() {
        next = 0;
    }
    IntSLLNode(int el, IntSLLNode *ptr = 0) {
        info = el; next = ptr;
    }
    int info;
    IntSLLNode *next;
};

class IntSLList {
public:
    IntSLList() {
        head = tail = 0;
    }
    ~IntSLList();
    int isEmpty() {
        return head == 0;
    }
    void addToHead(int);
    void addToTail(int);
    int  deleteFromHead(); // delete the head and return its info;
    int  deleteFromTail(); // delete the tail and return its info;
    void deleteNode(int);
    bool isInList(int) const;
    void printAll() const;
    
    //my functions
    
    bool isEmptyList() const;
    bool deleteAll() const; //returns true to indicate operation is complete.
    int listcount();
    
    void multiply(int multipler);
    void appendList(IntSLList* list02);
    int frequency(int val);
    void removeNod(int n,int k);
    
    //To-do
    /*  
     list1.reverse()
     List1.removeNod(n,k)
     List1.display()
     */
    
private:
    IntSLLNode *head, *tail;
};

#endif
