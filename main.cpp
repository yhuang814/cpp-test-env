//
//  main.cpp
//  oscar_assignment_01
//
//  Created by Born on 10/13/15.
//  Copyright © 2015 Born. All rights reserved.
//

#include <iostream>
#include "intSLList.h"

/**
 ntSLList.h functions:
 
 void addToHead(int);
 void addToTail(int);
 int  deleteFromHead(); // delete the head and return its info;
 int  deleteFromTail(); // delete the tail and return its info;
 void deleteNode(int);
 bool isInList(int) const;
 void printAll() const;
 
 
 //To-do
 /*
 list1.reverse()
 list1.deleteAll() - done
 list1.appendList(list2) - done
 list1.multiply(value) - done
 list1.frequency(value) - done
 list1.listcount() - done
 List1.removeNod(n,k) - done
 List1.display() - same as printAll()?
 */

using namespace std;

void getData(IntSLList& val, int size, int multiplier);

int main(int argc, const char * argv[]) {
    
    IntSLList list01, list02, emptyList;
    
    getData(list01,10, 1);
    getData(list02,10, 2);
    getData(emptyList,0, 1);
    
    
    cout << "list01: ";
    list01.printAll();
    
    cout << endl << "list02: ";
    list02.printAll();
    
    list01.appendList(&list02);

    cout << endl << "list01 + list02: ";
    list01.printAll();
    cout << "list01 Size: " << list01.listcount() << endl;
    
    cout << "Frequency: " << list01.frequency(0) << endl;
    
    list01.removeNod(1,3);
    cout << endl << "list01 + list02: ";
    list01.printAll();
    cout << "list01 Size: " << list01.listcount() << endl;

    
    //---------------
    cout << endl;
    return 0;
}

void getData(IntSLList& val, int size, int multiplier)
{
    for (int i = 0; i < size; i++) {
        
        val.addToTail(i*multiplier);
    }
}
